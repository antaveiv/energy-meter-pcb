EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 8
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 7100 2300 1650 2050
U 5DFD1AFA
F0 "CurrentSense" 50
F1 "CurrentSense.sch" 50
F2 "range10A" I L 7100 2400 50 
F3 "notRange10A" I L 7100 2550 50 
F4 "range1A" I L 7100 2700 50 
F5 "reference" I L 7100 3800 50 
F6 "range100mA" I L 7100 2850 50 
F7 "range10mA" I L 7100 3000 50 
F8 "range1mA" I L 7100 3150 50 
F9 "range100uA" I L 7100 3300 50 
F10 "currentMeas1" O L 7100 3950 50 
F11 "currentMeas2" O L 7100 4100 50 
F12 "DUT-" I R 8750 2400 50 
F13 "PSU-" I R 8750 2800 50 
$EndSheet
$Sheet
S 2600 2250 1150 600 
U 5E564C74
F0 "IsolatedSupply" 50
F1 "IsolatedSupply.sch" 50
F2 "IsolatedPower12V" I L 2600 2350 50 
F3 "IsolatedPowerGnd" I L 2600 2450 50 
$EndSheet
$Sheet
S 2600 3150 1150 900 
U 5E568147
F0 "IsolatedUart" 50
F1 "IsolatedUart.sch" 50
F2 "TX" I R 3750 3300 50 
F3 "RX" O R 3750 3400 50 
F4 "B" O L 2600 3600 50 
F5 "A" O L 2600 3500 50 
F6 "IsolatedSupply5V" O L 2600 3300 50 
F7 "IsolatedSupplyGnd" O L 2600 3400 50 
F8 "DE~RE" I R 3750 3900 50 
F9 "Trigger" O R 3750 4000 50 
F10 "IsolatedTrigger" I L 2600 3700 50 
$EndSheet
$Sheet
S 2600 4400 1150 900 
U 5E5681AD
F0 "IsolatedUSB" 50
F1 "IsolatedUSB.sch" 50
F2 "RX" O R 3750 4650 50 
F3 "TX" I R 3750 4550 50 
$EndSheet
$Sheet
S 4100 2700 2000 2050
U 5E56828B
F0 "MCU" 50
F1 "MCU.sch" 50
F2 "UartRx" I L 4100 3400 50 
F3 "UsbRx" I L 4100 4650 50 
F4 "UartTx" O L 4100 3300 50 
F5 "UsbTx" O L 4100 4550 50 
F6 "voltageMeas" I R 6100 4650 50 
F7 "DE~RE" O L 4100 3900 50 
F8 "range10A" O R 6100 2800 50 
F9 "range1A" O R 6100 3000 50 
F10 "notRange10A" O R 6100 2900 50 
F11 "range100mA" O R 6100 3100 50 
F12 "range10mA" O R 6100 3200 50 
F13 "range1mA" O R 6100 3300 50 
F14 "range100uA" O R 6100 3400 50 
F15 "reference" O R 6100 3800 50 
F16 "currentMeas1" I R 6100 3950 50 
F17 "currentMeas2" O R 6100 4100 50 
F18 "TriggerIn" I L 4100 4000 50 
$EndSheet
Wire Wire Line
	1150 3200 1250 3200
Wire Wire Line
	1250 3200 1250 3850
Wire Wire Line
	1150 3300 1350 3300
Wire Wire Line
	1350 3300 1350 3850
Wire Wire Line
	1150 3400 1450 3400
Wire Wire Line
	1450 3400 1450 3850
Wire Wire Line
	1150 3500 1550 3500
Wire Wire Line
	1550 3500 1550 3850
Wire Wire Line
	1150 3600 1650 3600
Wire Wire Line
	1650 3600 1650 3850
Wire Wire Line
	2600 2350 2250 2350
Wire Wire Line
	1250 2350 1250 3200
Connection ~ 1250 3200
Connection ~ 1350 3300
Wire Wire Line
	1450 3400 2400 3400
Wire Wire Line
	2400 3400 2600 3400
Connection ~ 1450 3400
Wire Wire Line
	2600 2450 2400 2450
Wire Wire Line
	2400 2450 2400 2800
Connection ~ 2400 3400
Wire Wire Line
	2600 3500 1550 3500
Connection ~ 1550 3500
Wire Wire Line
	2600 3600 1650 3600
Connection ~ 1650 3600
Text Notes 550  3700 0    59   ~ 0
+12V\n+5V\nGND\nA\nB\nTrigger
$Sheet
S 7100 4750 1650 1250
U 5E5C5B96
F0 "VoltageSense" 50
F1 "VoltageSense.sch" 50
F2 "EXT-" I R 8750 5500 50 
F3 "EXT+" I R 8750 5900 50 
F4 "voltageMeas" O L 7100 5200 50 
$EndSheet
$Comp
L Connector:Screw_Terminal_01x02 J?
U 1 1 5E6FDCE4
P 10600 2500
AR Path="/5DFD1AFA/5E6FDCE4" Ref="J?"  Part="1" 
AR Path="/5E6FDCE4" Ref="J1"  Part="1" 
F 0 "J1" H 10518 2267 50  0000 C CNN
F 1 "Screw_Terminal_01x02" H 10518 2266 50  0001 C CNN
F 2 "TerminalBlock:TerminalBlock_bornier-2_P5.08mm" H 10600 2500 50  0001 C CNN
F 3 "~" H 10600 2500 50  0001 C CNN
	1    10600 2500
	1    0    0    1   
$EndComp
$Comp
L Device:D D?
U 1 1 5E6FDCEB
P 10200 3100
AR Path="/5DFD1AFA/5E6FDCEB" Ref="D?"  Part="1" 
AR Path="/5E6FDCEB" Ref="D2"  Part="1" 
F 0 "D2" V 10200 3021 50  0000 R CNN
F 1 "STTH1L06A" V 10245 3179 50  0001 L CNN
F 2 "kicad-footprints-antaveiv:DO-214AC" H 10200 3100 50  0001 C CNN
F 3 "~" H 10200 3100 50  0001 C CNN
	1    10200 3100
	0    1    -1   0   
$EndComp
$Comp
L Device:D_TVS D?
U 1 1 5E6FDCF1
P 10000 3100
AR Path="/5DFD1AFA/5E6FDCF1" Ref="D?"  Part="1" 
AR Path="/5E6FDCF1" Ref="D1"  Part="1" 
F 0 "D1" V 10000 3021 50  0000 R CNN
F 1 "CGA0402MLC-24G" V 10045 3179 50  0001 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" H 10000 3100 50  0001 C CNN
F 3 "~" H 10000 3100 50  0001 C CNN
	1    10000 3100
	0    1    -1   0   
$EndComp
$Comp
L Connector:Screw_Terminal_01x02 J?
U 1 1 5E6FDCF7
P 10600 2900
AR Path="/5DFD1AFA/5E6FDCF7" Ref="J?"  Part="1" 
AR Path="/5E6FDCF7" Ref="J2"  Part="1" 
F 0 "J2" H 10518 2667 50  0000 C CNN
F 1 "Screw_Terminal_01x02" H 10518 2666 50  0001 C CNN
F 2 "TerminalBlock:TerminalBlock_bornier-2_P5.08mm" H 10600 2900 50  0001 C CNN
F 3 "~" H 10600 2900 50  0001 C CNN
	1    10600 2900
	1    0    0    1   
$EndComp
Wire Wire Line
	9700 4400 9700 2400
Connection ~ 9700 2400
Text Notes 7100 1900 0    50   ~ 0
Current sensing is done on low side. The PSU connection is not technically\nnecessary but it makes it more understandable to connect and provides a \nway to measure DUT voltage. External sense leads can be connected too,\nsee VoltageSense schematic sheet
Text Notes 10400 5950 1    50   ~ 0
External voltage sense connector
$Comp
L Mechanical:MountingHole H4
U 1 1 5ED44E79
P 900 7500
F 0 "H4" H 1000 7546 50  0000 L CNN
F 1 "MountingHole" H 1000 7455 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3" H 900 7500 50  0001 C CNN
F 3 "~" H 900 7500 50  0001 C CNN
	1    900  7500
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H3
U 1 1 5ED45575
P 900 7250
F 0 "H3" H 1000 7296 50  0000 L CNN
F 1 "MountingHole" H 1000 7205 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3" H 900 7250 50  0001 C CNN
F 3 "~" H 900 7250 50  0001 C CNN
	1    900  7250
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H2
U 1 1 5ED45ABC
P 900 7000
F 0 "H2" H 1000 7046 50  0000 L CNN
F 1 "MountingHole" H 1000 6955 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3" H 900 7000 50  0001 C CNN
F 3 "~" H 900 7000 50  0001 C CNN
	1    900  7000
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H1
U 1 1 5ED45CA0
P 900 6750
F 0 "H1" H 1000 6796 50  0000 L CNN
F 1 "MountingHole" H 1000 6705 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3" H 900 6750 50  0001 C CNN
F 3 "~" H 900 6750 50  0001 C CNN
	1    900  6750
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0104
U 1 1 5ED475FE
P 2250 7050
F 0 "#PWR0104" H 2250 6800 50  0001 C CNN
F 1 "GND" H 2255 6877 50  0000 C CNN
F 2 "" H 2250 7050 50  0001 C CNN
F 3 "" H 2250 7050 50  0001 C CNN
	1    2250 7050
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint_Alt TP51
U 1 1 5ED47AF9
P 2250 7000
F 0 "TP51" H 2308 7072 50  0000 L CNN
F 1 "TestPoint_Alt" H 2308 7027 50  0001 L CNN
F 2 "TestPoint:TestPoint_Pad_1.0x1.0mm" H 2450 7000 50  0001 C CNN
F 3 "~" H 2450 7000 50  0001 C CNN
	1    2250 7000
	1    0    0    -1  
$EndComp
Wire Wire Line
	2250 7000 2250 7050
$Comp
L power:GND #PWR0105
U 1 1 5ED4AD47
P 2600 7050
F 0 "#PWR0105" H 2600 6800 50  0001 C CNN
F 1 "GND" H 2605 6877 50  0000 C CNN
F 2 "" H 2600 7050 50  0001 C CNN
F 3 "" H 2600 7050 50  0001 C CNN
	1    2600 7050
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint_Alt TP52
U 1 1 5ED4AD4D
P 2600 7000
F 0 "TP52" H 2658 7072 50  0000 L CNN
F 1 "TestPoint_Alt" H 2658 7027 50  0001 L CNN
F 2 "TestPoint:TestPoint_Pad_1.0x1.0mm" H 2800 7000 50  0001 C CNN
F 3 "~" H 2800 7000 50  0001 C CNN
	1    2600 7000
	1    0    0    -1  
$EndComp
Wire Wire Line
	2600 7000 2600 7050
$Comp
L power:GND #PWR0106
U 1 1 5ED4BD8B
P 2950 7050
F 0 "#PWR0106" H 2950 6800 50  0001 C CNN
F 1 "GND" H 2955 6877 50  0000 C CNN
F 2 "" H 2950 7050 50  0001 C CNN
F 3 "" H 2950 7050 50  0001 C CNN
	1    2950 7050
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint_Alt TP53
U 1 1 5ED4BD91
P 2950 7000
F 0 "TP53" H 3008 7072 50  0000 L CNN
F 1 "TestPoint_Alt" H 3008 7027 50  0001 L CNN
F 2 "TestPoint:TestPoint_Pad_1.0x1.0mm" H 3150 7000 50  0001 C CNN
F 3 "~" H 3150 7000 50  0001 C CNN
	1    2950 7000
	1    0    0    -1  
$EndComp
Wire Wire Line
	2950 7000 2950 7050
$Comp
L power:GND #PWR0107
U 1 1 5ED4CE53
P 3300 7050
F 0 "#PWR0107" H 3300 6800 50  0001 C CNN
F 1 "GND" H 3305 6877 50  0000 C CNN
F 2 "" H 3300 7050 50  0001 C CNN
F 3 "" H 3300 7050 50  0001 C CNN
	1    3300 7050
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint_Alt TP54
U 1 1 5ED4CE59
P 3300 7000
F 0 "TP54" H 3358 7072 50  0000 L CNN
F 1 "TestPoint_Alt" H 3358 7027 50  0001 L CNN
F 2 "TestPoint:TestPoint_Pad_1.0x1.0mm" H 3500 7000 50  0001 C CNN
F 3 "~" H 3500 7000 50  0001 C CNN
	1    3300 7000
	1    0    0    -1  
$EndComp
Wire Wire Line
	3300 7000 3300 7050
$Comp
L power:GND #PWR0108
U 1 1 5ED4E0E2
P 3650 7050
F 0 "#PWR0108" H 3650 6800 50  0001 C CNN
F 1 "GND" H 3655 6877 50  0000 C CNN
F 2 "" H 3650 7050 50  0001 C CNN
F 3 "" H 3650 7050 50  0001 C CNN
	1    3650 7050
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint_Alt TP55
U 1 1 5ED4E0E8
P 3650 7000
F 0 "TP55" H 3708 7072 50  0000 L CNN
F 1 "TestPoint_Alt" H 3708 7027 50  0001 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x01_P2.54mm_Vertical" H 3850 7000 50  0001 C CNN
F 3 "~" H 3850 7000 50  0001 C CNN
	1    3650 7000
	1    0    0    -1  
$EndComp
Wire Wire Line
	3650 7000 3650 7050
Wire Wire Line
	4100 3300 3750 3300
Wire Wire Line
	3750 3400 4100 3400
Wire Wire Line
	4100 3900 3750 3900
Wire Wire Line
	3750 4000 4100 4000
Wire Wire Line
	4100 4550 3750 4550
Wire Wire Line
	3750 4650 4100 4650
Wire Wire Line
	6100 4650 6450 4650
Wire Wire Line
	6450 4650 6450 5200
Wire Wire Line
	6450 5200 7100 5200
Wire Wire Line
	6100 4100 7100 4100
Wire Wire Line
	7100 3950 6100 3950
Wire Wire Line
	6100 3800 7100 3800
Wire Wire Line
	6100 2800 6300 2800
Wire Wire Line
	6300 2800 6300 2400
Wire Wire Line
	6300 2400 7100 2400
Wire Wire Line
	6100 2900 6350 2900
Wire Wire Line
	6350 2900 6350 2550
Wire Wire Line
	6350 2550 7100 2550
Wire Wire Line
	6100 3000 6400 3000
Wire Wire Line
	6400 3000 6400 2700
Wire Wire Line
	6400 2700 7100 2700
Wire Wire Line
	6100 3100 6450 3100
Wire Wire Line
	6450 3100 6450 2850
Wire Wire Line
	6450 2850 7100 2850
Wire Wire Line
	6100 3200 6500 3200
Wire Wire Line
	6500 3200 6500 3000
Wire Wire Line
	6500 3000 7100 3000
Wire Wire Line
	6100 3300 6550 3300
Wire Wire Line
	6550 3300 6550 3150
Wire Wire Line
	6550 3150 7100 3150
Wire Wire Line
	6100 3400 6600 3400
Wire Wire Line
	6600 3400 6600 3300
Wire Wire Line
	6600 3300 7100 3300
Text Notes 10700 2500 0    50   ~ 0
DUT-\nDUT+\n
Text Notes 10700 2900 0    50   ~ 0
PSU IN -\nPSU IN+
Wire Wire Line
	9700 2400 10400 2400
Wire Wire Line
	9800 2500 10400 2500
Wire Wire Line
	8750 2800 10000 2800
Wire Wire Line
	10400 2900 10400 3300
Wire Wire Line
	10400 3300 10200 3300
Wire Wire Line
	9800 3300 9800 2500
Wire Wire Line
	10200 3250 10200 3300
Connection ~ 10200 3300
Wire Wire Line
	10200 3300 10000 3300
Wire Wire Line
	10000 3250 10000 3300
Connection ~ 10000 3300
Wire Wire Line
	10000 3300 9800 3300
Wire Wire Line
	10000 2950 10000 2800
Connection ~ 10000 2800
Wire Wire Line
	10000 2800 10200 2800
Wire Wire Line
	10200 2950 10200 2800
Connection ~ 10200 2800
Wire Wire Line
	10200 2800 10400 2800
$Comp
L Connector_Generic:Conn_01x02 J?
U 1 1 5F4BD621
P 10000 4500
AR Path="/5E5C5B96/5F4BD621" Ref="J?"  Part="1" 
AR Path="/5F4BD621" Ref="J13"  Part="1" 
F 0 "J13" H 10080 4446 50  0000 L CNN
F 1 "Conn_01x02" H 9918 4266 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 10000 4500 50  0001 C CNN
F 3 "~" H 10000 4500 50  0001 C CNN
	1    10000 4500
	1    0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J?
U 1 1 5F4BD627
P 9500 4500
AR Path="/5E5C5B96/5F4BD627" Ref="J?"  Part="1" 
AR Path="/5F4BD627" Ref="J14"  Part="1" 
F 0 "J14" H 9418 4267 50  0000 C CNN
F 1 "Conn_01x02" H 9418 4266 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 9500 4500 50  0001 C CNN
F 3 "~" H 9500 4500 50  0001 C CNN
	1    9500 4500
	-1   0    0    1   
$EndComp
Connection ~ 9800 3300
Wire Wire Line
	8750 2400 9700 2400
$Comp
L Graphic:Logo_Open_Hardware_Large #LOGO1
U 1 1 5E2787AF
P 6050 7200
F 0 "#LOGO1" H 6050 7700 50  0001 C CNN
F 1 "Logo_Open_Hardware_Large" H 6050 6800 50  0001 C CNN
F 2 "Symbol:OSHW-Logo_11.4x12mm_SilkScreen" H 6050 7200 50  0001 C CNN
F 3 "~" H 6050 7200 50  0001 C CNN
	1    6050 7200
	1    0    0    -1  
$EndComp
Wire Wire Line
	9800 3300 9800 4400
$Comp
L power:GND #PWR0147
U 1 1 5E23C5A5
P 4000 7050
F 0 "#PWR0147" H 4000 6800 50  0001 C CNN
F 1 "GND" H 4005 6877 50  0000 C CNN
F 2 "" H 4000 7050 50  0001 C CNN
F 3 "" H 4000 7050 50  0001 C CNN
	1    4000 7050
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint_Alt TP48
U 1 1 5E23C5AF
P 4000 7000
F 0 "TP48" H 4058 7072 50  0000 L CNN
F 1 "TestPoint_Alt" H 4058 7027 50  0001 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x01_P2.54mm_Vertical" H 4200 7000 50  0001 C CNN
F 3 "~" H 4200 7000 50  0001 C CNN
	1    4000 7000
	1    0    0    -1  
$EndComp
Wire Wire Line
	4000 7000 4000 7050
$Sheet
S 2600 5650 1150 450 
U 5E16FCC2
F0 "NegativeSupply" 50
F1 "NegativeSupply.sch" 50
$EndSheet
$Comp
L Connector:Screw_Terminal_01x02 J?
U 1 1 5E191D41
P 10150 5600
AR Path="/5DFD1AFA/5E191D41" Ref="J?"  Part="1" 
AR Path="/5E191D41" Ref="J5"  Part="1" 
F 0 "J5" H 10068 5367 50  0000 C CNN
F 1 "Screw_Terminal_01x02" H 10068 5366 50  0001 C CNN
F 2 "TerminalBlock:TerminalBlock_bornier-2_P5.08mm" H 10150 5600 50  0001 C CNN
F 3 "~" H 10150 5600 50  0001 C CNN
	1    10150 5600
	1    0    0    1   
$EndComp
$Comp
L Device:D_TVS D?
U 1 1 5E191D47
P 9050 5700
AR Path="/5DFD1AFA/5E191D47" Ref="D?"  Part="1" 
AR Path="/5E191D47" Ref="D16"  Part="1" 
F 0 "D16" V 9050 5621 50  0000 R CNN
F 1 "CGA0402MLC-24G" V 9095 5779 50  0001 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" H 9050 5700 50  0001 C CNN
F 3 "~" H 9050 5700 50  0001 C CNN
	1    9050 5700
	0    1    -1   0   
$EndComp
$Comp
L Device:D D?
U 1 1 5E191D4D
P 9200 5700
AR Path="/5DFD1AFA/5E191D4D" Ref="D?"  Part="1" 
AR Path="/5E191D4D" Ref="D27"  Part="1" 
F 0 "D27" V 9200 5621 50  0000 R CNN
F 1 "STTH1L06A" V 9245 5779 50  0001 L CNN
F 2 "kicad-footprints-antaveiv:DO-214AC" H 9200 5700 50  0001 C CNN
F 3 "~" H 9200 5700 50  0001 C CNN
	1    9200 5700
	0    1    -1   0   
$EndComp
Wire Wire Line
	9050 5550 9050 5500
Wire Wire Line
	9200 5550 9200 5500
Connection ~ 9200 5500
Wire Wire Line
	9200 5500 9050 5500
Wire Wire Line
	9800 5900 9650 5900
Wire Wire Line
	9050 5850 9050 5900
Connection ~ 9050 5900
Wire Wire Line
	9050 5900 8750 5900
Wire Wire Line
	9200 5850 9200 5900
Connection ~ 9200 5900
Wire Wire Line
	9200 5900 9050 5900
$Comp
L Device:R R66
U 1 1 5E191D5F
P 9500 5500
F 0 "R66" V 9293 5500 50  0000 C CNN
F 1 "10K" V 9384 5500 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 9430 5500 50  0001 C CNN
F 3 "~" H 9500 5500 50  0001 C CNN
	1    9500 5500
	0    1    1    0   
$EndComp
Wire Wire Line
	9350 5500 9200 5500
$Comp
L Device:R R67
U 1 1 5E191D66
P 9500 5900
F 0 "R67" V 9293 5900 50  0000 C CNN
F 1 "10K" V 9384 5900 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 9430 5900 50  0001 C CNN
F 3 "~" H 9500 5900 50  0001 C CNN
	1    9500 5900
	0    1    1    0   
$EndComp
Wire Wire Line
	9350 5900 9200 5900
Connection ~ 9050 5500
Wire Wire Line
	9950 5600 9800 5600
Wire Wire Line
	9800 5600 9800 5900
Wire Wire Line
	9650 5500 9700 5500
Text Notes 10200 5650 0    50   ~ 0
-\n+
Wire Wire Line
	8750 5500 9050 5500
$Comp
L Connector:Barrel_Jack_Switch J?
U 1 1 5E257DC6
P 1750 2700
AR Path="/5E564C74/5E257DC6" Ref="J?"  Part="1" 
AR Path="/5E257DC6" Ref="J11"  Part="1" 
F 0 "J11" H 1807 3017 50  0000 C CNN
F 1 "Barrel_Jack_Switch" H 1807 2926 50  0000 C CNN
F 2 "Connector_BarrelJack:BarrelJack_Wuerth_6941xx301002" H 1800 2660 50  0001 C CNN
F 3 "~" H 1800 2660 50  0001 C CNN
	1    1750 2700
	1    0    0    -1  
$EndComp
Wire Wire Line
	2050 2800 2400 2800
Connection ~ 2400 2800
Wire Wire Line
	2400 2800 2400 3400
Wire Wire Line
	2050 2600 2250 2600
Wire Wire Line
	2250 2600 2250 2350
Connection ~ 2250 2350
Wire Wire Line
	2250 2350 1250 2350
$Comp
L Connector_Generic:Conn_01x06 J3
U 1 1 5E2F0834
P 950 3500
F 0 "J3" H 868 2975 50  0000 C CNN
F 1 "Conn_01x06" H 868 3066 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x06_P2.54mm_Vertical" H 950 3500 50  0001 C CNN
F 3 "~" H 950 3500 50  0001 C CNN
	1    950  3500
	-1   0    0    1   
$EndComp
Wire Wire Line
	1150 3700 1750 3700
$Comp
L Connector_Generic:Conn_01x06 J4
U 1 1 5E3061C4
P 1550 4050
F 0 "J4" V 1422 4330 50  0000 L CNN
F 1 "Conn_01x06" V 1513 4330 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x06_P2.54mm_Vertical" H 1550 4050 50  0001 C CNN
F 3 "~" H 1550 4050 50  0001 C CNN
	1    1550 4050
	0    1    1    0   
$EndComp
Wire Wire Line
	1750 3850 1750 3700
Connection ~ 1750 3700
Wire Wire Line
	1750 3700 2600 3700
Connection ~ 9700 5500
Wire Wire Line
	9700 5500 9950 5500
Connection ~ 9800 5600
Wire Wire Line
	9800 4500 9800 5600
Wire Wire Line
	9700 4500 9700 5500
NoConn ~ 2050 2700
Wire Wire Line
	1350 3300 2600 3300
$EndSCHEMATC
